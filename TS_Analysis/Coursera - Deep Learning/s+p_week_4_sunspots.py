## envset 
# TF2.0
%tensorflow_version 2.x
import tensorflow as tf
from tensorflow import keras as keras
# import tensorflow_probability as tfp

import os
import numpy as np
import pandas as pd
import datetime as dt

# Plotting
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import seaborn as sns; sns.set()


## Get data
# !curl "https://www.datastro.eu/explore/dataset/daily-sunspot-number/download/?format=csv&timezone=Africa/Cairo&use_labels_for_header=true&csv_separator=%3B" -o sunspots.csv

## Or
!wget -O sunspots.csv --no-check-certificate --progress=dot "https://raw.githubusercontent.com/gaberm/ML-Journey/master/Datasets/Sunspots.csv" 

# !head sunspots.csv
# df = pd.read_csv('sunspots.csv', parse_dates=['Date'], index_col='Date', sep=",")
os.system('head sunspots.csv')

def plot_series(time, series, format="-", start=0, end=None, label=None, title=None):
    plt.plot(time[start:end], series[start:end], format, label=label)
    plt.xlabel("Time")
    plt.ylabel("Value")
    plt.legend(loc='upper right', borderaxespad=0.)
    plt.title(title)
    plt.grid(False)


import csv
time_step, sunspots, date =[], [], []

with open('sunspots.csv') as file:
  reader = csv.reader(file, delimiter=',')
  next(reader)
  for row in reader:
    time_step.append(int(row[0]))
    date.append(row[1])
    sunspots.append(float(row[2]))

series=np.array(sunspots)
time_step = np.array(time_step)
date = np.array(date)

plt.figure(figsize=(10,6))
plot_series(time_step, series, label='sunspots')

"""Tuning"""
## Splitting 

split_time = 3000
time_train = time_step[:split_time]
x_train = series[:split_time]
time_valid = time_step[split_time:]
x_valid = series[split_time:]

window_size = 30
batch_size = 32
shuffle_buffer_size = 1000

def windowed_dataset(series, window_size, batch_size, shuffle_buffer):
    series = tf.expand_dims(series, axis=-1)
    ds = tf.data.Dataset.from_tensor_slices(series)
    ds = ds.window(window_size + 1, shift=1, drop_remainder=True)
    ds = ds.flat_map(lambda w: w.batch(window_size + 1))
    ds = ds.shuffle(shuffle_buffer)
    ds = ds.map(lambda w: (w[:-1], w[1:]))
    return ds.batch(batch_size).prefetch(1)

def model_forecast(model, series, window_size):
    ds = tf.data.Dataset.from_tensor_slices(series)
    ds = ds.window(window_size, shift=1, drop_remainder=True)
    ds = ds.flat_map(lambda w: w.batch(window_size))
    ds = ds.batch(32).prefetch(1)
    forecast = model.predict(ds)
    return forecast

tf.keras.backend.clear_session()
tf.random.set_seed(51)
np.random.seed(51)
window_size = 64
batch_size = 256
train_set = windowed_dataset(x_train, window_size, batch_size, shuffle_buffer_size)
print(train_set)
print(x_train.shape)

model = tf.keras.models.Sequential([
  tf.keras.layers.Conv1D(filters=32, kernel_size=5,
                      strides=1, padding="causal",
                      activation="relu",
                      input_shape=[None, 1]),
  tf.keras.layers.LSTM(64, return_sequences=True),
  tf.keras.layers.LSTM(64, return_sequences=True),
  tf.keras.layers.Dense(30, activation="relu"),
  tf.keras.layers.Dense(10, activation="relu"),
  tf.keras.layers.Dense(1),
  tf.keras.layers.Lambda(lambda x: x * 400)
])

lr_schedule = tf.keras.callbacks.LearningRateScheduler(
    lambda epoch: 1e-8 * 10**(epoch / 20))
optimizer = tf.keras.optimizers.SGD(lr=1e-8, momentum=0.9)
model.compile(loss=tf.keras.losses.Huber(),
              optimizer=optimizer,
              metrics=["mae"])
history = model.fit(train_set, epochs=100, callbacks=[lr_schedule])

plt.semilogx(history.history["lr"], history.history["loss"])
plt.axis([1e-8, 1e-4, 0, 60])

"""Forecasting"""
tf.keras.backend.clear_session()
tf.random.set_seed(51)
np.random.seed(51)
train_set = windowed_dataset(x_train, window_size=60, batch_size=100, shuffle_buffer=shuffle_buffer_size)
model = tf.keras.models.Sequential([
  tf.keras.layers.Conv1D(filters=60, kernel_size=5,
                      strides=1, padding="causal",
                      activation="relu",
                      input_shape=[None, 1]),
  tf.keras.layers.LSTM(60, return_sequences=True),
  tf.keras.layers.LSTM(60, return_sequences=True),
  tf.keras.layers.Dense(30, activation="relu"),
  tf.keras.layers.Dense(10, activation="relu"),
  tf.keras.layers.Dense(1),
  tf.keras.layers.Lambda(lambda x: x * 400)
])


optimizer = tf.keras.optimizers.SGD(lr=1e-5, momentum=0.9)
model.compile(loss=tf.keras.losses.Huber(),
              optimizer=optimizer,
              metrics=["mae"])
history = model.fit(train_set,epochs=500, verbose=0)

rnn_forecast = model_forecast(model, series[..., np.newaxis], window_size)
rnn_forecast = rnn_forecast[split_time - window_size:-1, -1, 0]

# Errors
mse = tf.keras.metrics.mean_absolute_error(x_valid, rnn_forecast).numpy()
mape = tf.keras.metrics.MeanAbsolutePercentageError()
_ = mape.update_state(x_valid, rnn_forecast)


plt.figure(figsize=(10, 6))
plot_series(time_valid, x_valid, label='actual')
plot_series(time_valid, rnn_forecast, label='forecast')
plt.annotate('LR={:.3e}'.format(1e-5),
             xy=(.8, .75), xycoords='figure fraction')
plt.annotate('MAE={:.3f}'.format(mse),
             xy=(.8, .8), xycoords='figure fraction')
plt.annotate('MAPE={:.3f}'.format(mape.result().numpy()),
             xy=(.8, .85), xycoords='figure fraction')
plt.show()


#-----------------------------------------------------------
# Retrieve a list of list results on training and test data
# sets for each training epoch
#-----------------------------------------------------------
loss=history.history['loss']

epochs=range(len(loss)) # Get number of epochs


#------------------------------------------------
# Plot training and validation loss per epoch
#------------------------------------------------
plt.plot(epochs, loss, 'r')
plt.title('Training loss')
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend(["Loss"])

plt.figure()



zoomed_loss = loss[200:]
zoomed_epochs = range(200,500)


#------------------------------------------------
# Plot training and validation loss per epoch
#------------------------------------------------
plt.plot(zoomed_epochs, zoomed_loss, 'r')
plt.title('Training loss')
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend(["Loss"])

plt.figure()