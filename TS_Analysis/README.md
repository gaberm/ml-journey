# Study of Time Series Analysis & Forecasting Methods

## [Time Series Analysis - Intro](https://github.com/gaberm/ML-Journey/blob/master/TS_Analysis/Time_Series_Analysis.ipynb)
## [Modelling & Stationary Data](https://github.com/gaberm/ML-Journey/blob/master/TS_Analysis/Modelling%20and%20Stationary%20Data.ipynb)


## [Time Series Analysis - using ARIMA](https://github.com/gaberm/ML-Journey/blob/master/TS_Analysis/TSA_Forecasting_Using_ARIMA.ipynb)

## [Time Series Analysis - Introduction to Exponential Smoothing](https://github.com/gaberm/ML-Journey/blob/master/TS_Analysis/An_Introduction_to_Exponential_Smoothing_.ipynb)
## [Time Series Analysis - using LSTM](https://github.com/gaberm/ML-Journey/blob/master/TS_Analysis/TSA_Forecasting_Using_LSTM.ipynb)

## Deep Learning

### Deep Learning for Time Series Analysis

## Exercises
### [Forecasting Principles & Practices - Air Passengers](https://github.com/gaberm/ML-Journey/blob/master/TS_Analysis/Forecasting_TSA_Decomposition.ipynb)

### Deep Learning TS Exercises